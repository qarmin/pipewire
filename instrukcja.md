
### Compilation in Docker

```
docker run -it ubuntu:rolling /bin/bash

apt update

apt install -y git

git clone https://gitlab.freedesktop.org/pipewire/pipewire.git
cd pipewire

sed -Ei 's/^# deb-src /deb-src /' /etc/apt/sources.list

export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true

apt update

#Z#
apt build-dep -y pipewire
apt install -y libjack-jackd2-dev libbluetooth-dev libvulkan-dev

meson build/

ninja -C build/

```

### QTCreator Includes
```
spa/include
src/
```
